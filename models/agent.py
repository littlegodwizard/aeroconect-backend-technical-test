from fastapi import Depends
from pydantic import BaseModel
from pymongo.collection import Collection

from db.mongodb import get_collection
from models.helpers.mixin import IDModelMixin, DateTimeModelMixin
from models.helpers.utils import BSONObjectID
from models.merchant import merchantInProductReturn


class marginFee(BaseModel):
    percent: float
    amount: float

    def get_margin(self, price):
        return (self.percent * price) + self.amount


class agentInDB(IDModelMixin, DateTimeModelMixin):
    name: str
    merchantID: BSONObjectID
    marginFee: marginFee


class agentInProductReturn(BaseModel):
    name: str
    merchant: merchantInProductReturn
    marginFee: marginFee

    def get_margin(self, price):
        return self.marginFee.get_margin(price)


class agentDao:
    def __init__(self):
        self.dao: Collection = Depends(get_collection("agents")).dependency

    def save(self, data: agentInDB):
        data.set_date()
        data = data.dict(exclude_none=True)
        insert = self.dao.insert_one(data)
        return insert.acknowledged
