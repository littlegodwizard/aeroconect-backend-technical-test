from typing import List

from fastapi import Depends
from pydantic import BaseModel
from pymongo.collection import Collection

from db.mongodb import get_collection
from models.helpers.mixin import IDModelMixin, DateTimeModelMixin
from models.helpers.utils import BSONObjectID, RWModel
from models.agent import agentInProductReturn


class price(BaseModel):
    sku: str
    name: str
    price: float


class productInDB(IDModelMixin, DateTimeModelMixin):
    name: str
    inventoryID: BSONObjectID
    prices: List[price]


class productReturn(RWModel):
    name: str
    sku: str
    variant: str
    price: float
    agent: agentInProductReturn


class productDao:
    def __init__(self):
        self.dao: Collection = Depends(get_collection("products")).dependency

    def save(self, data: productInDB):
        data.set_date()
        data = data.dict(exclude_none=True)
        insert = self.dao.insert_one(data)
        return insert.acknowledged

    def get_all_product(self):
        pipeline = [
            {"$unwind": "$prices"},
            {"$lookup": {
                "from": "agents",
                "let": {"inventoryID": "$inventoryID"},
                "pipeline": [
                    {"$match": {"$expr": {"$eq": ["$_id", "$$inventoryID"]}}},
                    {"$lookup": {
                        "from": "merchants",
                        "let": {"merchantID": "$merchantID"},
                        "pipeline": [
                            {"$match": {"$expr": {"$eq": ["$_id", "$$merchantID"]}}},
                            {"$project": {
                                "_id": 0,
                                "name": "$name",
                                "managementFee": "$managementFee",
                                "merchantFee": "$merchantFee"
                            }}
                        ],
                        "as": "merchant"
                    }},
                    {"$project": {
                        "_id": 0,
                        "name": "$name",
                        "merchant": {"$arrayElemAt": ["$merchant", 0]},
                        "marginFee": "$marginFee"
                    }}
                ],
                "as": "agent"
            }},
            {"$project": {
                "_id": 0,
                "name": "$name",
                "sku": "$prices.sku",
                "variant": "$prices.name",
                "price": "$prices.price",
                "agent": {"$arrayElemAt": ["$agent", 0]}
            }}
        ]

        query = self.dao.aggregate(pipeline)
        result = []
        if query is not None:
            for a in query:
                data = productReturn(**a)
                result.append(data)
        return result
