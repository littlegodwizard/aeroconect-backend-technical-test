from fastapi import Depends
from pydantic import BaseModel
from pymongo.collection import Collection
from db.mongodb import get_collection
from models.helpers.mixin import DateTimeModelMixin, IDModelMixin


class managementFee(BaseModel):
    percent: float
    amount: float

    def get_fee_aero(self, selling_price):
        return ((self.percent / 100) * selling_price) + self.amount


class merchantFee(BaseModel):
    percent: float
    amount: float

    def get_csr_association(self, selling_price):
        return ((self.percent / 100) * selling_price) + self.amount


class merchantInDB(IDModelMixin, DateTimeModelMixin):
    name: str
    managementFee: managementFee
    merchantFee: merchantFee


class merchantInProductReturn(BaseModel):
    name: str
    managementFee: managementFee
    merchantFee: merchantFee

    def get_fee_aero(self, selling_price):
        return self.managementFee.get_fee_aero(selling_price)

    def get_csr_association(self, selling_price):
        return self.merchantFee.get_csr_association(selling_price)


class merchantDao:
    def __init__(self):
        self.dao: Collection = Depends(get_collection("merchants")).dependency

    def save(self, data: merchantInDB):
        data.set_date()
        data = data.dict(exclude_none=True)
        insert = self.dao.insert_one(data)
        return insert.acknowledged
