from datetime import datetime
from bson.objectid import ObjectId
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, BaseConfig
from starlette.responses import JSONResponse


def create_aliased_response(model: BaseModel) -> JSONResponse:
    return JSONResponse(content=jsonable_encoder(model, by_alias=True))


class BSONObjectID(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(
            pattern='/^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i',
            type='string',
            examples=['5e585d687e8c126c614a2fd2', '5e585d687e8c126c614a2fd3'],
        )

    @classmethod
    def validate(cls, v):
        if isinstance(v, ObjectId):
            return str(v)
        elif isinstance(v, str) and ObjectId.is_valid(v):
            return ObjectId(v)
        else:
            raise TypeError('ObjectId required')


class RWModel(BaseModel):
    def dict(self, *arg, **args):
        return super().dict(by_alias=True,
                            include=None,
                            exclude=None,
                            skip_defaults=None,
                            exclude_unset=False,
                            exclude_defaults=False,
                            exclude_none=True
                            )

    class Config(BaseConfig):
        orm_mode = True
        allow_population_by_alias = True
        arbitrary_types_allowed = True
        json_encoders = {
            datetime: lambda dt: int(dt.timestamp() * 1000),
            ObjectId: lambda objectid: str(objectid)
        }