import datetime
from typing import Optional
from pydantic import Field, validator
from models.helpers.utils import BSONObjectID, RWModel


class DateTimeModelMixin(RWModel):
    created_at: datetime.datetime = None  # type: ignore
    updated_at: datetime.datetime = None  # type: ignore

    @validator("created_at", "updated_at", pre=True)
    def default_datetime(
            cls,  # noqa: N805
            value: datetime.datetime,  # noqa: WPS110
    ) -> datetime.datetime:
        return value or datetime.datetime.now()

    def set_date(self):
        self.updated_at = self.default_datetime(self.updated_at)
        self.created_at = self.default_datetime(self.created_at)


class IDModelMixin(RWModel):
    id: Optional[BSONObjectID] = Field(None, alias='_id')