Web routes
----------

All routes available on ``/docs`` or ``/redoc`` paths with Swagger or ReDoc.


Project structure
-----------------
    root
    ├── api                     - web related stuff.
    │   └── routes              - web routes.
    ├── core                    - application configuration, startup events, logging.
    ├── db                      - db related stuff.
    │   ├── mongodb.py          - monggodb connection.
    ├── dump                    - database dump
    ├── models                  - pydantic models for this application consist database schemas and query.
    ├── modules                 - modules helper.
    ├── resources               - string and media stuff used in web responses.
    ├── services                - logic.
    └── main.py                 - FastAPI application creation and configuration.
    
FRAMEWORKS
-----------------
- this project using fastapi framework https://fastapi.tiangolo.com/
- as for the database I use mongodb

PYTHON VERSION
-----------------
python 3.6


RUN PROJECT IN LOCALHOST - PREPARATION
-----------------
1. installing mongodb
2. crteate database "aero-connect" or and store dump DB (optional)
3. installing python
4. installing python package manager (I use pip)
5. clone project
6. change directory to root project and install dependencies ``/pip install -r requirements.txt``


RUN DEV
-----------------
1. running mongodb server
3. running app in command line using this command "uvicorn main:app --reload"
4. your app will serve in localhost
  
