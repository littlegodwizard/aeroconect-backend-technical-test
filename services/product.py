from models.product import productDao, productInDB


class detailProductInReturn:
    sku: str
    name: str
    price: float
    margin: float
    selling_price: float
    fee_aero: float
    csr_association: float
    total_fee: float
    fee_association: float
    merchant: str
    agent: str

    def set_value(self, name, value):
        setattr(self, name, value)


class serviceProduct:
    @staticmethod
    def save_merchant(request: productInDB):
        return productDao().save(request)

    @staticmethod
    def get_all_product():

        product_list = productDao().get_all_product()

        result = []

        for data in product_list:
            product = detailProductInReturn()
            # set sku
            product.set_value("sku", data.sku)

            # set name
            product.set_value("name", data.name + " (" + data.variant + ")")

            # set price
            product.set_value("price", data.price)

            # set agent
            product.set_value("agent", data.agent.name)

            # set merchant
            product.set_value("merchant", data.agent.merchant.name)

            # set margin
            margin = data.agent.get_margin(data.price)
            product.set_value("margin", margin)

            # set selling price
            selling_price = data.price + margin
            product.set_value("selling_price", selling_price)

            # set fee aero
            fee_aero = data.agent.merchant.get_fee_aero(selling_price)
            product.set_value("fee_aero", fee_aero)

            # set csr association
            csr_association = data.agent.merchant.get_csr_association(selling_price)
            product.set_value("csr_association", csr_association)

            # set total fee
            total_fee = fee_aero + csr_association
            product.set_value("total_fee", total_fee)

            # set fee association
            fee_association = (margin - total_fee) + csr_association
            product.set_value("fee_association", fee_association)

            result.append(product)

        return result
