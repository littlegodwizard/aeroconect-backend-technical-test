from fastapi import APIRouter
from services.merchant import serviceMerchant
from models.merchant import merchantInDB

router = APIRouter()


@router.post("/add")
async def add_merchant(request: merchantInDB):
    return serviceMerchant().save_merchant(request)
